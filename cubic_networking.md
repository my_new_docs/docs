#resolve no ping within cubic chroot env

mkdir /run/systemd/resolve/

echo "nameserver 1.1.1.1 " | tee /run/systemd/resolve/resolv.conf

ln -sr /run/systemd/resolve/resolv.conf /run/systemd/resolve/stub-resolv.conf

cat /etc/resolv.conf

/etc/init.d/networking restart 

ping https://www.archlinux.org/

#fix missing packages

add universe repo to /etc/apt/sources.list

sudo add-apt-repository universe multiverse